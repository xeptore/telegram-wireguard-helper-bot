package table_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/xeptore/telegram-wireguard-helper/table"
)

var (
	inputMultiple = []table.PeerRow{
		{
			ID:            5316,
			IsActive:      true,
			Name:          "Treachery",
			PublicKey:     "zuScZ9cMYyod7mYqOYk5...",
			AllowedIPs:    "10.0.0.1/32",
			UploadBytes:   667.97 * 1_000_000,
			DownloadBytes: 7.06 * 1_000_000_000,
		},
		{
			ID:            5317,
			IsActive:      true,
			Name:          "Goldsmith",
			PublicKey:     "5ohPFRkszr0jv9GteJUZ...",
			AllowedIPs:    "10.0.0.2/32",
			UploadBytes:   227.28 * 1_000_000,
			DownloadBytes: 2.88 * 1_000_000_000,
		},
		{
			ID:            5318,
			IsActive:      false,
			Name:          "Scarce",
			PublicKey:     "/ueiVF+4NmWPrI+cG5Y4...",
			AllowedIPs:    "10.0.0.3/32",
			UploadBytes:   20.96 * 1_000_000,
			DownloadBytes: 200.58 * 1_000_000,
		},
		{
			ID:            5319,
			IsActive:      true,
			Name:          "Preformed",
			PublicKey:     "PN8ZFVn7IWkY+E73O6qe...",
			AllowedIPs:    "10.0.0.4/32",
			UploadBytes:   293.25 * 1_000_000,
			DownloadBytes: 2.57 * 1_000_000_000,
		},
		{
			ID:            5320,
			IsActive:      false,
			Name:          "Swimmable",
			PublicKey:     "rHuczvH4sZAwhn5n9VMA...",
			AllowedIPs:    "10.0.0.5/32",
			UploadBytes:   415.32 * 1_000_000,
			DownloadBytes: 2.55 * 1_000_000_000,
		},
		{
			ID:            5321,
			IsActive:      false,
			Name:          "Suffice",
			PublicKey:     "QmVgUkVYSTJkeBv+9vO3...",
			AllowedIPs:    "10.0.0.6/32",
			UploadBytes:   561.98 * 1_000_000,
			DownloadBytes: 2.48 * 1_000_000_000,
		},
		{
			ID:            5322,
			IsActive:      false,
			Name:          "Epiphany",
			PublicKey:     "/G0Pz+bxAEada8XOX5NV...",
			AllowedIPs:    "10.0.0.7/32",
			UploadBytes:   185.80 * 1_000_000,
			DownloadBytes: 2.32 * 1_000_000_000,
		},
		{
			ID:            5323,
			IsActive:      true,
			Name:          "Debtless",
			PublicKey:     "GhNqvHmgC/Vd6SPGyja0...",
			AllowedIPs:    "10.0.0.8/32",
			UploadBytes:   67.50 * 1_000_000,
			DownloadBytes: 605.55 * 1_000_000,
		},
		{
			ID:            5324,
			IsActive:      true,
			Name:          "Startle",
			PublicKey:     "2fxMFviNipqghQt14kik...",
			AllowedIPs:    "10.0.0.9/32",
			UploadBytes:   105.62 * 1_000_000,
			DownloadBytes: 451.16 * 1_000_000,
		},
		{
			ID:            5325,
			IsActive:      false,
			Name:          "Perky",
			PublicKey:     "5b65yNNZastKc0prV3V0...",
			AllowedIPs:    "10.0.0.10/32",
			UploadBytes:   32.66 * 1_000_000,
			DownloadBytes: 728.42 * 1_000_000,
		},
	}
	expectedMultipleOutput = `┌─────────────────────────┐
│           ID            │
├─────────────────────────┤
│        IsActive         │
├─────────────────────────┤
│          Name           │
├─────────────────────────┤
│       Allowed IPs       │
├───────────┬─────────────┤
│  Upload   │  Download   │
├───────────┴─────────────┤
│       Public Key        │
╞═════════════════════════╡
│          5316           │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│        Treachery        │
├─────────────────────────┤
│       10.0.0.1/32       │
├────────────┬────────────┤
│ 667.97 MB  │  7.06 GB   │
├────────────┴────────────┤
│ zuScZ9cMYyod7mYqOYk5... │
╞═════════════════════════╡
│          5317           │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│        Goldsmith        │
├─────────────────────────┤
│       10.0.0.2/32       │
├────────────┬────────────┤
│ 227.28 MB  │  2.88 GB   │
├────────────┴────────────┤
│ 5ohPFRkszr0jv9GteJUZ... │
╞═════════════════════════╡
│          5318           │
├─────────────────────────┤
│        INACTIVE         │
├─────────────────────────┤
│         Scarce          │
├─────────────────────────┤
│       10.0.0.3/32       │
├────────────┬────────────┤
│  20.96 MB  │ 200.58 MB  │
├────────────┴────────────┤
│ /ueiVF+4NmWPrI+cG5Y4... │
╞═════════════════════════╡
│          5319           │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│        Preformed        │
├─────────────────────────┤
│       10.0.0.4/32       │
├────────────┬────────────┤
│ 293.25 MB  │  2.57 GB   │
├────────────┴────────────┤
│ PN8ZFVn7IWkY+E73O6qe... │
╞═════════════════════════╡
│          5320           │
├─────────────────────────┤
│        INACTIVE         │
├─────────────────────────┤
│        Swimmable        │
├─────────────────────────┤
│       10.0.0.5/32       │
├────────────┬────────────┤
│ 415.32 MB  │  2.55 GB   │
├────────────┴────────────┤
│ rHuczvH4sZAwhn5n9VMA... │
╞═════════════════════════╡
│          5321           │
├─────────────────────────┤
│        INACTIVE         │
├─────────────────────────┤
│         Suffice         │
├─────────────────────────┤
│       10.0.0.6/32       │
├────────────┬────────────┤
│ 561.98 MB  │  2.48 GB   │
├────────────┴────────────┤
│ QmVgUkVYSTJkeBv+9vO3... │
╞═════════════════════════╡
│          5322           │
├─────────────────────────┤
│        INACTIVE         │
├─────────────────────────┤
│        Epiphany         │
├─────────────────────────┤
│       10.0.0.7/32       │
├────────────┬────────────┤
│ 185.80 MB  │  2.32 GB   │
├────────────┴────────────┤
│ /G0Pz+bxAEada8XOX5NV... │
╞═════════════════════════╡
│          5323           │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│        Debtless         │
├─────────────────────────┤
│       10.0.0.8/32       │
├────────────┬────────────┤
│  67.50 MB  │ 605.55 MB  │
├────────────┴────────────┤
│ GhNqvHmgC/Vd6SPGyja0... │
╞═════════════════════════╡
│          5324           │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│         Startle         │
├─────────────────────────┤
│       10.0.0.9/32       │
├────────────┬────────────┤
│ 105.62 MB  │ 451.16 MB  │
├────────────┴────────────┤
│ 2fxMFviNipqghQt14kik... │
╞═════════════════════════╡
│          5325           │
├─────────────────────────┤
│        INACTIVE         │
├─────────────────────────┤
│          Perky          │
├─────────────────────────┤
│      10.0.0.10/32       │
├────────────┬────────────┤
│  32.66 MB  │ 728.42 MB  │
├────────────┴────────────┤
│ 5b65yNNZastKc0prV3V0... │
└─────────────────────────┘`
	inputSingle = []table.PeerRow{
		{
			ID:            0,
			IsActive:      true,
			Name:          "Treachery",
			PublicKey:     "zuScZ9cMYyod7mYqOYk5...",
			AllowedIPs:    "10.0.0.1/32",
			UploadBytes:   124.42 * 1_000_000,
			DownloadBytes: 861.42 * 1_000_000,
		},
	}
	expectedSingleOutput = `┌─────────────────────────┐
│           ID            │
├─────────────────────────┤
│        IsActive         │
├─────────────────────────┤
│          Name           │
├─────────────────────────┤
│       Allowed IPs       │
├───────────┬─────────────┤
│  Upload   │  Download   │
├───────────┴─────────────┤
│       Public Key        │
╞═════════════════════════╡
│            0            │
├─────────────────────────┤
│         ACTIVE          │
├─────────────────────────┤
│        Treachery        │
├─────────────────────────┤
│       10.0.0.1/32       │
├────────────┬────────────┤
│ 124.42 MB  │ 861.42 MB  │
├────────────┴────────────┤
│ zuScZ9cMYyod7mYqOYk5... │
└─────────────────────────┘`
	inputLongName = []table.PeerRow{
		{
			ID:            0,
			IsActive:      true,
			Name:          "Strive Amber Twilight Statutory Sniff Manpower Dizzy",
			PublicKey:     "zuScZ9cMYyod7mYqOYk5...",
			AllowedIPs:    "10.0.0.1/32",
			UploadBytes:   227.28 * 1_000_000,
			DownloadBytes: 2.88 * 1_000_000_000,
		},
	}
	expectedLongNameOutput = `┌──────────────────────────────────────────────────────┐
│                          ID                          │
├──────────────────────────────────────────────────────┤
│                       IsActive                       │
├──────────────────────────────────────────────────────┤
│                         Name                         │
├──────────────────────────────────────────────────────┤
│                     Allowed IPs                      │
├──────────────────────────┬───────────────────────────┤
│          Upload          │         Download          │
├──────────────────────────┴───────────────────────────┤
│                      Public Key                      │
╞══════════════════════════════════════════════════════╡
│                          0                           │
├──────────────────────────────────────────────────────┤
│                        ACTIVE                        │
├──────────────────────────────────────────────────────┤
│ Strive Amber Twilight Statutory Sniff Manpower Dizzy │
├──────────────────────────────────────────────────────┤
│                     10.0.0.1/32                      │
├──────────────────────────┬───────────────────────────┤
│        227.28 MB         │          2.88 GB          │
├──────────────────────────┴───────────────────────────┤
│               zuScZ9cMYyod7mYqOYk5...                │
└──────────────────────────────────────────────────────┘`
)

func TestRender(t *testing.T) {
	t.Parallel()

	t.Run("WithManyRows", func(t *testing.T) {
		t.Parallel()
		renderWithManyRows(t)
	})

	t.Run("WithSingleRow", func(t *testing.T) {
		t.Parallel()
		renderWithSingleRow(t)
	})

	t.Run("WithNoRows", func(t *testing.T) {
		t.Parallel()
		renderWithNoRows(t)
	})
}

func renderWithManyRows(t *testing.T) {
	rows := inputMultiple
	text := table.Render(rows)

	require.Equal(t, expectedMultipleOutput, string(text))
}

func renderWithSingleRow(t *testing.T) {
	rows := inputSingle
	text := table.Render(rows)

	require.Equal(t, expectedSingleOutput, string(text))

	rows = inputLongName
	text = table.Render(rows)

	require.Equal(t, expectedLongNameOutput, string(text))
}

func TestCalcCap(t *testing.T) {
	t.Parallel()

	t.Run("WithManyRows", func(t *testing.T) {
		t.Parallel()
		calcCapWithManyRows(t)
	})

	t.Run("WithSingleRow", func(t *testing.T) {
		t.Parallel()
		calcCapWithSingleRow(t)
	})

	t.Run("WithNoRows", func(t *testing.T) {
		t.Parallel()
		calcCapWithNoRows(t)
	})
}

func calcCapWithNoRows(t *testing.T) {
	n := table.CalcCap([]table.PeerRow{})

	require.Equal(t, 0, n)
}

func calcCapWithSingleRow(t *testing.T) {
	rows := inputLongName
	n := table.CalcCap(rows)
	expectedLen := len(expectedLongNameOutput)
	require.Equal(t, expectedLen, n)

	rows = inputSingle
	n = table.CalcCap(rows)
	expectedLen = len(expectedSingleOutput)
	require.Equal(t, expectedLen, n)
}

func calcCapWithManyRows(t *testing.T) {
	rows := inputMultiple
	n := table.CalcCap(rows)
	expectedLen := len(expectedMultipleOutput)
	require.Equal(t, expectedLen, n)
}

func renderWithNoRows(t *testing.T) {
	b := table.Render([]table.PeerRow{})

	const expected string = ""
	require.Equal(t, expected, string(b))
}

func TestBytesToHuman(t *testing.T) {
	t.Parallel()
	require.Equal(t, "349 B", table.BytesToHuman(349))
	require.Equal(t, "18 B", table.BytesToHuman(18))
	require.Equal(t, "9 B", table.BytesToHuman(9))
	require.Equal(t, "0 B", table.BytesToHuman(0))
	require.Equal(t, "7.06 GB", table.BytesToHuman(7.06*1_000_000_000))
	require.Equal(t, "637.97 MB", table.BytesToHuman(637.97*1_000_000))
	require.Equal(t, "22.20 GB", table.BytesToHuman(22.2*1_000_000_000))
	require.Equal(t, "2.96 KB", table.BytesToHuman(2.96*1_000))
	require.Equal(t, "293.25 GB", table.BytesToHuman(293.25*1_000_000_000))
	require.Equal(t, "415.30 KB", table.BytesToHuman(415.3*1_000))
	require.Equal(t, "5.90 GB", table.BytesToHuman(5.9*1_000_000_000))
}

var (
	benchRes []byte
)

func BenchmarkRenderBytes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rows := inputMultiple
		benchRes = table.Render(rows)
	}
}

var (
	s = ""
)

func BenchmarkBytesToHuman(b *testing.B) {
	b.StopTimer()
	nums := []int64{
		349,
		18,
		9,
		0,
		637.97 * 1_000_000,
		22.2 * 1_000_000_000,
		2.96 * 1_000,
		293.25 * 1_000_000_000,
		415.3 * 1_000,
		5.9 * 1_000_000_000,
	}
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		for _, v := range nums {
			s = table.BytesToHuman(v)
		}
	}
}
