package table

import (
	"fmt"
	"strconv"
)

type PeerRow struct {
	ID            int
	IsActive      bool
	Name          string
	PublicKey     string
	AllowedIPs    string
	UploadBytes   int64
	DownloadBytes int64
}

func calcWidth(rows []PeerRow) int {
	width := func() int {
		max := 0
		for _, r := range rows {
			if l := len(r.AllowedIPs); l > max {
				max = l
			}
			if l := len(r.Name); l > max {
				max = l
			}
			if l := len(r.PublicKey); l > max {
				max = l
			}
		}
		return max
	}()
	width += 2 // some padding to look better
	return width
}

func CalcCap(rows []PeerRow) int {
	if len(rows) == 0 {
		return 0
	}

	width := calcWidth(rows)

	endingLine := (width + 2) * 3
	headerRow := (6 * (width + 2) * 3) + (5 * (width + 2*3)) + (width - 1 + 3*3)
	perRow := (5 * (width + 2*3)) + (width - 1 + 3*3) + (6 * (width + 2) * 3)
	newLines := (len(rows) + 1) * 12

	return perRow*len(rows) + endingLine + newLines + headerRow
}

func Render(rows []PeerRow) []byte {
	if len(rows) == 0 {
		return nil
	}

	const (
		ColNameID         = "ID"
		ColNameIsActive   = "IsActive"
		ColNameName       = "Name"
		ColNameAllowedIPs = "Allowed IPs"
		ColNameUpload     = "Upload"
		ColNameDownload   = "Download"
		ColNamePublicKey  = "Public Key"
	)

	width := calcWidth(rows)

	b := make([]byte, 0, CalcCap(rows))

	repeat := func(char []byte, n int) {
		for i := 0; i < n; i++ {
			b = append(b, char...)
		}
	}

	b = append(b, []byte("┌")...)
	repeat([]byte("─"), width)
	b = append(b, []byte("┐\n│")...)

	left, right := spaces(ColNameID, width)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameID)...)
	repeat([]byte(" "), right)

	b = append(b, []byte("│\n├")...)
	repeat([]byte("─"), width)
	b = append(b, []byte("┤\n│")...)

	left, right = spaces(ColNameIsActive, width)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameIsActive)...)
	repeat([]byte(" "), right)

	b = append(b, []byte("│\n├")...)
	repeat([]byte("─"), width)
	b = append(b, []byte("┤\n│")...)

	left, right = spaces(ColNameName, width)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameName)...)
	repeat([]byte(" "), right)

	b = append(b, []byte("│\n├")...)
	repeat([]byte("─"), width)
	b = append(b, []byte("┤\n│")...)

	left, right = spaces(ColNameAllowedIPs, width)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameAllowedIPs)...)
	repeat([]byte(" "), right)

	b = append(b, []byte("│\n├")...)
	repeat([]byte("─"), width/2-1)
	b = append(b, []byte("┬")...)
	repeat([]byte("─"), width/2+(width%2^0))
	b = append(b, []byte("┤\n│")...)

	left, right = spaces(ColNameUpload, width/2-width%2)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameUpload)...)
	repeat([]byte(" "), right-(width%2^1))

	b = append(b, []byte("│")...)

	l := width/2 + width%2
	left, right = spaces(ColNameDownload, l)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNameDownload)...)
	repeat([]byte(" "), right)

	b = append(b, []byte("│\n├")...)
	repeat([]byte("─"), width/2-1)
	b = append(b, []byte("┴")...)
	repeat([]byte("─"), width/2+(width%2^0))
	b = append(b, []byte("┤\n│")...)

	left, right = spaces(ColNamePublicKey, width)
	repeat([]byte(" "), left)
	b = append(b, []byte(ColNamePublicKey)...)
	repeat([]byte(" "), right)
	b = append(b, []byte("│\n")...)

	for _, v := range rows {
		b = append(b, []byte("╞")...)
		repeat([]byte("═"), width)
		b = append(b, []byte("╡\n│")...)

		idx := strconv.Itoa(v.ID)
		left, right = spaces(idx, width)
		repeat([]byte(" "), left)
		b = append(b, []byte(idx)...)
		repeat([]byte(" "), right)

		b = append(b, []byte("│\n├")...)
		repeat([]byte("─"), width)
		b = append(b, []byte("┤\n│")...)

		if v.IsActive {
			const active = "ACTIVE"
			left, right = spaces(active, width)
			repeat([]byte(" "), left)
			b = append(b, []byte(active)...)
			repeat([]byte(" "), right)
		} else {
			const inactive = "INACTIVE"
			left, right = spaces(inactive, width)
			repeat([]byte(" "), left)
			b = append(b, []byte(inactive)...)
			repeat([]byte(" "), right)
		}

		b = append(b, []byte("│\n├")...)
		repeat([]byte("─"), width)
		b = append(b, []byte("┤\n│")...)

		left, right = spaces(v.Name, width)
		repeat([]byte(" "), left)
		b = append(b, []byte(v.Name)...)
		repeat([]byte(" "), right)

		b = append(b, []byte("│\n├")...)
		repeat([]byte("─"), width)
		b = append(b, []byte("┤\n│")...)

		left, right = spaces(v.AllowedIPs, width)
		repeat([]byte(" "), left)
		b = append(b, []byte(v.AllowedIPs)...)
		repeat([]byte(" "), right)

		b = append(b, []byte("│\n├")...)
		repeat([]byte("─"), width/2-(width%2^1))
		b = append(b, []byte("┬")...)
		repeat([]byte("─"), width/2)
		b = append(b, []byte("┤\n│")...)

		upload := BytesToHuman(v.UploadBytes)
		left, right = spaces(upload, width/2-(width%2^1))
		repeat([]byte(" "), left)
		b = append(b, []byte(upload)...)
		repeat([]byte(" "), right)

		b = append(b, []byte("│")...)

		download := BytesToHuman(v.DownloadBytes)
		left, right = spaces(download, width/2)
		repeat([]byte(" "), left)
		b = append(b, []byte(download)...)
		repeat([]byte(" "), right)

		b = append(b, []byte("│\n├")...)
		repeat([]byte("─"), width/2-(width%2^1))
		b = append(b, []byte("┴")...)
		repeat([]byte("─"), width/2)
		b = append(b, []byte("┤\n│")...)

		left, right = spaces(v.PublicKey, width)
		repeat([]byte(" "), left)
		// This loop can be replaced with append like previous ones,
		// but it will cause an allocation when casting to []byte.
		// This prevents that unnecessary allocation.
		for i := 0; i < len(v.PublicKey); i++ {
			b = append(b, v.PublicKey[i])
		}
		repeat([]byte(" "), right)

		b = append(b, []byte("│\n")...)
	}

	b = append(b, []byte("└")...)
	repeat([]byte("─"), width)
	b = append(b, []byte("┘")...)
	return b
}

func spaces(text string, width int) (int, int) {
	if rem := width - len(text); rem%2 == 0 {
		return rem / 2, rem / 2
	} else {
		return rem / 2, rem/2 + 1
	}
}

func BytesToHuman(b int64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}

	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.2f %cB", float64(b)/float64(div), "KMGTPE"[exp])
}
