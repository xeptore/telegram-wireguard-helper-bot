package main

import (
	"context"
	"fmt"
	"net/netip"
	"strings"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
)

func (h *Handler) handleSetDNSCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	arg := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandSetDNS))
	parts := strings.SplitN(arg, ",", 2)
	var dnsAddrs []string
	for _, p := range parts {
		addr, err := netip.ParseAddr(p)
		if nil != err || !addr.IsValid() || !addr.Is4() || addr.IsUnspecified() || !addr.IsGlobalUnicast() || addr.IsInterfaceLocalMulticast() || addr.IsLinkLocalMulticast() || addr.IsLoopback() || addr.IsMulticast() {
			_, err := b.SendMessage(ctx, &bot.SendMessageParams{
				ChatID:    chatID,
				Text:      fmt.Sprintf("Use `%s dns1,dns2` message format.\n_Using 1 DNS address is recommended._", CommandSetDNS),
				ParseMode: ParseModeMarkdownV1,
			})
			if nil != err {
				h.logger.Error().Err(err).Msg("failed to send command help message")
			}
			return
		}
		dnsAddrs = append(dnsAddrs, addr.String())
	}

	if err := h.store.SetDNS(ctx, dnsAddrs); nil != err {
		h.logger.Error().Err(err).Msg("failed to set DNS addresses")
		if _, err = b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		}); nil != err {
			h.logger.Error().Err(err).Msg("failed to send failure error reply message")
			return
		}
		return
	}
	if _, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		ReplyToMessageID: msgID,
		Text:             fmt.Sprintf("✅ You should regenerate each peer config using %s command for changes to be applied.", CommandGetPeer),
	}); nil != err {
		h.logger.Error().Err(err).Msg("failed to send success reply message")
		return
	}
}
