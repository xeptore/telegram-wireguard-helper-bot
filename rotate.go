package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
)

func (h *Handler) handleSRotateKeysCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandRotateKeys))
	peerIndex, err := strconv.Atoi(args)
	if len(strings.TrimSpace(args)) == 0 || nil != err {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s peer_id` message format.", CommandRotateKeys),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	}
	h.logger.Debug().Str("args", args).Msg("processing rotate peer keys request")

	peer, err := h.store.Peer(ctx, peerIndex)
	if nil != err {
		if errors.Is(err, os.ErrNotExist) {
			_, err := b.SendMessage(ctx, &bot.SendMessageParams{
				ChatID: chatID,
				Text:   "No peers found",
			})
			if nil != err {
				h.logger.Error().Err(err).Msg("failed to send command failure message")
			}
			return
		}
		h.logger.Error().Err(err).Msg("failed to get peer info from store")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Failed to retrieve peer from store:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}

		return
	}

	prvPubKey, err := wgtypes.ParseKey(peer.PublicKey)
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to parse peer public key")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Failed to parse peer public key:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}
	err = h.wg.ConfigureDevice(h.wgDeviceName(), wgtypes.Config{
		ReplacePeers: false,
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey: prvPubKey,
				Remove:    true,
			},
		},
	})
	if nil != err && !errors.Is(err, os.ErrNotExist) {
		h.logger.Error().Err(err).Msg("failed to remove peer from WireGuard device")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Failed to remove existing peer from WireGuard peers:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}

	privateKey, err := wgtypes.GeneratePrivateKey()
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to generate peer private key")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("failed to generate peer private key:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}

	psk, err := wgtypes.GenerateKey()
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to generate peer preshared key")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("failed to generate peer preshared key:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}

	if err := h.store.RotatePeerKeys(ctx, peerIndex, privateKey.String(), privateKey.PublicKey().String(), psk.String()); nil != err {
		h.logger.Error().Err(err).Msg("failed to rotate peer keys")
		if _, err = b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		}); nil != err {
			h.logger.Error().Err(err).Msg("failed to send failure error reply message")
			return
		}
		return
	}

	_, ipNet, err := net.ParseCIDR(peer.AllowedIPs.IPv4 + "/32")
	if nil != err {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Failed to add peer to WireGuard device:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}
	allowedIPs := []net.IPNet{*ipNet}

	err = h.wg.ConfigureDevice(h.wgDeviceName(), wgtypes.Config{
		ReplacePeers: false,
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey:         privateKey.PublicKey(),
				PresharedKey:      &psk,
				AllowedIPs:        allowedIPs,
				ReplaceAllowedIPs: true,
			},
		},
	})
	if nil != err && !errors.Is(err, os.ErrNotExist) {
		h.logger.Error().Err(err).Msg("failed to add peer to WireGuard device")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Failed to add peer to WireGuard device:\n\n```\n%v\n```", err),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command failure message")
		}
		return
	}
	_, err = b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             "✅",
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send success reply message")
		return
	}
}
