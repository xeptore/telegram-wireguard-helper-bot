package main

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/urfave/cli/v2"

	"gitlab.com/xeptore/telegram-wireguard-helper/db"
)

func buildDump(log zerolog.Logger) func(ctx *cli.Context) error {
	return func(ctx *cli.Context) error {
		store, err := db.New(ctx.Context, ctx.String(CLIFlagDB))
		if nil != err {
			log.Fatal().Err(err).Msg("failed to initialize database")
		}
		if err := store.DumpPeers(ctx.Context, os.Stdout); nil != err {
			log.Fatal().Err(err).Msg("failed to dump peers list to stdout")
		}
		return nil
	}
}
