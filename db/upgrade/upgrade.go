package upgrade

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"math"
	"os"

	"github.com/goccy/go-json"
	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/rs/zerolog"
	"github.com/tidwall/gjson"
	"github.com/xeptore/wireuse/pkg/funcutils"

	"gitlab.com/xeptore/telegram-wireguard-helper/db"
	"gitlab.com/xeptore/telegram-wireguard-helper/db/version"
)

func Run(ctx context.Context, log zerolog.Logger, dbFilePath string) (err error) {
	dbContent, err := os.ReadFile(dbFilePath)
	if nil != err {
		return fmt.Errorf("failed to read db file: %v", err)
	}

	dbVersion, err := currentVersion(dbContent)
	if nil != err {
		if errors.Is(err, ErrUnknownVersion) {
			return errors.New("failed to detect current db file version")
		}
		return fmt.Errorf("failed to detect current db file version: %v", err)
	}
	if dbVersion == version.Current {
		return ErrAlreadyLatest
	} else if dbVersion > version.Current || dbVersion < 1 {
		return ErrUnknownVersion
	}

	suffix, err := gonanoid.New(64)
	if nil != err {
		return fmt.Errorf("failed to generate random name for db backup file which usually should be fixed by rerunning the operation: %v", err)
	}
	backupFileName := os.TempDir() + string(os.PathSeparator) + suffix + ".db"
	if err := os.WriteFile(backupFileName, dbContent, db.FilePerm); nil != err {
		return fmt.Errorf("failed to write db file content to backup file: %v", err)
	}

	newContent, err := upgrade(ctx, upgradeFns[dbVersion-1:], dbContent)
	if nil != err {
		return fmt.Errorf("failed to upgrade db content to latest version from version %d: %v", dbVersion, err)
	}

	if err := os.WriteFile(backupFileName, newContent, db.FilePerm); nil != err {
		return fmt.Errorf("failed to write new db content to backup file: %v", err)
	}

	if err := os.WriteFile(dbFilePath, newContent, db.FilePerm); nil != err {
		return fmt.Errorf("failed to write new db content to db file: %v", err)
	}

	if err := os.Remove(backupFileName); nil != err {
		return fmt.Errorf("failed to delete backup file: %v", err)
	}

	return nil
}

type upgradeFn func(ctx context.Context, content []byte) (upgradedContent []byte, err error)

var upgradeFns = []upgradeFn{
	upgradeV1ToV2,
	upgradeV2ToV3,
}

func upgrade(ctx context.Context, fns []upgradeFn, content []byte) ([]byte, error) {
	mem := content
	for i, fn := range fns {
		c, err := fn(ctx, mem)
		if nil != err {
			return nil, fmt.Errorf("failed to run upgrade function %d: %v", i, err)
		}
		mem = c
	}

	return mem, nil
}

func upgradeV1ToV2(ctx context.Context, currentContent []byte) ([]byte, error) {
	decoder := json.NewDecoder(bytes.NewBuffer(currentContent))
	decoder.DisallowUnknownFields()
	var peers version.V1DBSchema
	if err := decoder.DecodeContext(ctx, &peers); nil != err {
		return nil, fmt.Errorf("failed to unmarshal v1 db file content: %v", err)
	}

	v2DB := version.V2DBSchema{
		Peers:   peers,
		Version: 2,
		Settings: version.V2DBSettings{
			DNSAddrs: []string{"172.16.0.1"},
		},
	}

	var buf bytes.Buffer
	encoder := json.NewEncoder(&buf)
	if err := encoder.EncodeContext(ctx, v2DB); nil != err {
		return nil, fmt.Errorf("failed to marshal upgraded v2 db content: %v", err)
	}

	return buf.Bytes(), nil
}

func upgradeV2ToV3(ctx context.Context, currentContent []byte) ([]byte, error) {
	decoder := json.NewDecoder(bytes.NewBuffer(currentContent))
	decoder.DisallowUnknownFields()
	var db version.V2DBSchema
	if err := decoder.DecodeContext(ctx, &db); nil != err {
		return nil, fmt.Errorf("failed to unmarshal v2 db file content: %v", err)
	}

	v3Peers := funcutils.Map(db.Peers, func(p version.V1Peer) version.V3Peer {
		return version.V3Peer{
			PrivateKey:      p.PrivateKey,
			PublicKey:       p.PublicKey,
			PSK:             p.PSK,
			DateCreated:     p.DateCreated,
			IsActive:        p.IsActive,
			Name:            p.Name,
			AllowedIPs:      p.AllowedIPs,
			PreviousPubKeys: []string{},
		}
	})

	v3DB := version.V3DBSchema{
		Peers:   v3Peers,
		Version: 3,
		Settings: version.V2DBSettings{
			DNSAddrs: []string{"172.16.0.1"},
		},
	}

	var buf bytes.Buffer
	encoder := json.NewEncoder(&buf)
	if err := encoder.EncodeContext(ctx, v3DB); nil != err {
		return nil, fmt.Errorf("failed to marshal upgraded v3 db content: %v", err)
	}

	return buf.Bytes(), nil
}

func currentVersion(content []byte) (int, error) {
	res := gjson.ParseBytes(content)
	if res.Type == gjson.JSON && res.IsArray() {
		return 1, nil
	}
	if res.Type == gjson.JSON && res.IsObject() {
		versionKey := res.Get("version")
		if versionKey.Exists() {
			if versionKey.Type == gjson.Number {
				if i := versionKey.Int(); i <= math.MaxInt {
					return int(i), nil
				} else {
					return -1, fmt.Errorf("got version number greater than maximum expected value: %d", i)
				}
			}
			return -1, fmt.Errorf("unexpected value for version key: %s", versionKey.Raw)
		}
		return -1, errors.New("expected version key to exists")
	}

	return -1, ErrUnknownVersion
}

var (
	ErrUnknownVersion = errors.New("could not detect current database version")
	ErrAlreadyLatest  = errors.New("database is already at the latest version")
)
