package version

type (
	V2Peer           = V1Peer
	V2PeerAllowedIPs = V1PeerAllowedIPs
)

type V2DBSettings struct {
	DNSAddrs []string `json:"dns"`
}

type V2DBSchema struct {
	Peers    []V2Peer     `json:"peers"`
	Settings V2DBSettings `json:"settings"`
	Version  int          `json:"version"`
}
