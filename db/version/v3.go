package version

type V3Peer struct {
	PrivateKey      string           `json:"privateKey"`
	PublicKey       string           `json:"publicKey"`
	PSK             string           `json:"psk"`
	DateCreated     string           `json:"dateCreated"`
	IsActive        bool             `json:"isActive"`
	Name            string           `json:"name"`
	AllowedIPs      V1PeerAllowedIPs `json:"allowedIPs"`
	PreviousPubKeys []string         `json:"previousPublicKeys"`
}

type V3DBSchema struct {
	Peers    []V3Peer     `json:"peers"`
	Settings V2DBSettings `json:"settings"`
	Version  int          `json:"version"`
}
