package version

type V1PeerAllowedIPs struct {
	IPv4 string `json:"ipv4"`
}

type V1Peer struct {
	PrivateKey  string           `json:"privateKey"`
	PublicKey   string           `json:"publicKey"`
	PSK         string           `json:"psk"`
	DateCreated string           `json:"dateCreated"`
	IsActive    bool             `json:"isActive"`
	Name        string           `json:"name"`
	AllowedIPs  V1PeerAllowedIPs `json:"allowedIPs"`
}

type V1DBSchema []V1Peer
