package db

import (
	"context"
	"errors"
	"fmt"
	"io"
	"math"
	"net"
	"os"

	"github.com/goccy/go-json"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/ini.v1"

	"gitlab.com/xeptore/telegram-wireguard-helper/db/version"
)

const (
	FilePerm = 0644
)

type DB struct {
	filePath string
	data     version.V3DBSchema
}

type (
	Peer           = version.V3Peer
	PeerAllowedIPs = version.V2PeerAllowedIPs
)

func New(ctx context.Context, dbFilePath string) (*DB, error) {
	content, err := os.ReadFile(dbFilePath)
	if nil != err && !errors.Is(err, os.ErrNotExist) {
		return nil, fmt.Errorf("failed to initialize db file: %w", err)
	}

	if len(content) == 0 {
		return &DB{dbFilePath, version.V3DBSchema{
			Settings: version.V2DBSettings{
				DNSAddrs: []string{"172.16.0.1"},
			},
			Version: version.Current,
		}}, nil
	}

	var data version.V3DBSchema
	if err := json.UnmarshalContext(ctx, content, &data); nil != err {
		return nil, fmt.Errorf("failed to parse stored db file content: %v", err)
	}

	return &DB{dbFilePath, data}, nil
}

func (db *DB) persist(ctx context.Context) error {
	b, err := json.MarshalContext(ctx, db.data)
	if nil != err {
		return fmt.Errorf("failed to marshal db data: %v", err)
	}
	if err := os.WriteFile(db.filePath, b, FilePerm); nil != err {
		return fmt.Errorf("failed to persist db data: %w", err)
	}
	return nil
}

func (db *DB) DNS() []string {
	return db.data.Settings.DNSAddrs
}

func (db *DB) SetDNS(ctx context.Context, dnsAddrs []string) error {
	db.data.Settings.DNSAddrs = dnsAddrs
	return db.persist(ctx)
}

func (db *DB) AppendPeer(ctx context.Context, peer Peer) error {
	db.data.Peers = append(db.data.Peers, peer)
	return db.persist(ctx)
}

func (db *DB) DeactivatePeer(ctx context.Context, id int) error {
	peersCount := len(db.data.Peers)
	if id > peersCount {
		return os.ErrNotExist
	}

	db.data.Peers[id].IsActive = false

	return db.persist(ctx)
}

func (db *DB) ActivatePeer(ctx context.Context, id int) error {
	peersCount := len(db.data.Peers)
	if id > peersCount {
		return os.ErrNotExist
	}

	db.data.Peers[id].IsActive = true

	return db.persist(ctx)
}

func (db *DB) RenamePeer(ctx context.Context, id int, newName string) (prevName string, err error) {
	peersCount := len(db.data.Peers)
	if id > peersCount {
		return "", os.ErrNotExist
	}

	prevName = db.data.Peers[id].Name

	db.data.Peers[id].Name = newName

	return prevName, db.persist(ctx)
}

func (db *DB) RotatePeerKeys(ctx context.Context, id int, privateKey, publicKey, psk string) error {
	peersCount := len(db.data.Peers)
	if id > peersCount {
		return os.ErrNotExist
	}

	previousPublicKeys := make([]string, 0, len(db.data.Peers[id].PreviousPubKeys)+1)
	previousPublicKeys = append(previousPublicKeys, db.data.Peers[id].PreviousPubKeys...)
	previousPublicKeys = append(previousPublicKeys, db.data.Peers[id].PublicKey)

	db.data.Peers[id].PreviousPubKeys = previousPublicKeys
	db.data.Peers[id].PrivateKey = privateKey
	db.data.Peers[id].PublicKey = publicKey
	db.data.Peers[id].PSK = psk

	return db.persist(ctx)
}

func (db *DB) LastPeerIPs(ctx context.Context) (*PeerAllowedIPs, error) {
	if l := len(db.data.Peers); l == 0 {
		return nil, os.ErrNotExist
	} else {
		return &PeerAllowedIPs{
			IPv4: db.data.Peers[l-1].AllowedIPs.IPv4,
		}, nil
	}
}

func (db *DB) Peer(ctx context.Context, id int) (*Peer, error) {
	peersCount := len(db.data.Peers)
	if id > peersCount {
		return nil, os.ErrNotExist
	}

	peer := db.data.Peers[id]
	out := Peer{
		PrivateKey:  peer.PrivateKey,
		PublicKey:   peer.PublicKey,
		PSK:         peer.PSK,
		DateCreated: peer.DateCreated,
		IsActive:    peer.IsActive,
		Name:        peer.Name,
		AllowedIPs:  peer.AllowedIPs,
	}

	return &out, nil
}

type PaginationOpt struct {
	pageNo   int
	pageSize int
}

type PaginationOptFunc func(*PaginationOpt)

func WithPageNo(pageNo int) PaginationOptFunc {
	return func(opt *PaginationOpt) {
		opt.pageNo = pageNo
	}
}

func WithPageSize(pageSize int) PaginationOptFunc {
	return func(opt *PaginationOpt) {
		opt.pageSize = pageSize
	}
}

type PeerListItem struct {
	ID int
	Peer
}

type PeerListResult struct {
	Items      []PeerListItem
	Total      int
	TotalPages int
}

func (db *DB) Peers(ctx context.Context, paginationOpts ...PaginationOptFunc) (*PeerListResult, error) {
	opt := PaginationOpt{
		pageNo:   0,
		pageSize: 10,
	}
	for _, f := range paginationOpts {
		f(&opt)
	}

	total := len(db.data.Peers)
	if total == 0 {
		return nil, os.ErrNotExist
	}
	totalPages := int(math.Ceil(float64(total) / float64(opt.pageSize)))
	if opt.pageNo >= totalPages {
		return nil, os.ErrNotExist
	}

	offset := opt.pageNo * opt.pageSize
	limit := min(total, (opt.pageNo+1)*opt.pageSize)
	items := make([]PeerListItem, 0, limit)
	for i, p := range db.data.Peers[offset:limit] {
		items = append(items, PeerListItem{
			Peer: p,
			ID:   i + offset,
		})
	}

	return &PeerListResult{Items: items, Total: total, TotalPages: totalPages}, nil
}

func (db *DB) DumpPeers(ctx context.Context, wr io.Writer) error {
	for i, p := range db.data.Peers {
		if !p.IsActive {
			continue
		}

		cfg := ini.Empty()
		peerSec := cfg.Section("Peer")
		peerSec.NewKey("PublicKey", p.PublicKey)
		peerSec.NewKey("PresharedKey", p.PSK)
		peerSec.NewKey("AllowedIPs", fmt.Sprintf("%s/32", p.AllowedIPs.IPv4))

		if _, err := cfg.WriteTo(wr); nil != err {
			return fmt.Errorf("failed to serialize wireguard server peer %d config: %v", i, err)
		}
	}

	return nil
}

func (db *DB) WGActivePeers(ctx context.Context) ([]wgtypes.PeerConfig, error) {
	var out []wgtypes.PeerConfig
	for _, p := range db.data.Peers {
		if !p.IsActive {
			continue
		}

		pubKey, err := wgtypes.ParseKey(p.PublicKey)
		if nil != err {
			return nil, fmt.Errorf("failed to parse peer public key: %v", err)
		}

		psk, err := wgtypes.ParseKey(p.PSK)
		if nil != err {
			return nil, fmt.Errorf("failed to parse peer preshared key: %v", err)
		}

		_, ipNet, err := net.ParseCIDR(p.AllowedIPs.IPv4 + "/32")
		if nil != err {
			return nil, fmt.Errorf("failed to parse peer ipv4 address: %v", err)
		}
		allowedIPs := []net.IPNet{*ipNet}

		out = append(out, wgtypes.PeerConfig{
			PublicKey:         pubKey,
			Remove:            false,
			UpdateOnly:        false,
			PresharedKey:      &psk,
			ReplaceAllowedIPs: true,
			AllowedIPs:        allowedIPs,
		})
	}

	return out, nil
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
