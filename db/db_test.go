package db_test

import (
	"bytes"
	"context"
	"errors"
	"os"
	"strings"
	"testing"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/stretchr/testify/require"

	"gitlab.com/xeptore/telegram-wireguard-helper/db"
)

func createTemp(t *testing.T) (*os.File, error) {
	id, err := gonanoid.New()
	require.Nil(t, err)
	return os.CreateTemp(t.TempDir(), strings.ReplaceAll(t.Name(), "/", "_")+id)
}

func TestNew(t *testing.T) {
	t.Parallel()
	t.Run("Invalid DB File Content", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		n, err := f.WriteString(".")
		require.Nil(t, err)
		require.Equal(t, 1, n)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, store)
		require.NotNil(t, err)
	})

	t.Run("DB File Read Error", func(t *testing.T) {
		t.Parallel()
		ctx := context.Background()
		store, err := db.New(ctx, t.TempDir())
		require.Nil(t, store)
		require.NotNil(t, err)
	})
}

func TestPeer(t *testing.T) {
	t.Parallel()
	t.Run("From Empty", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		peer, err := store.Peer(ctx, 4)
		require.ErrorIs(t, err, os.ErrNotExist)
		require.Nil(t, peer)
	})

	t.Run("From Non-Empty", func(t *testing.T) {
		t.Parallel()
		t.Run("Not Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			peer, err := store.Peer(ctx, 4)
			require.ErrorIs(t, err, os.ErrNotExist)
			require.Nil(t, peer)
		})

		t.Run("Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			peer, err := store.Peer(ctx, 0)
			require.Nil(t, err)
			require.Equal(t, &db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			}, peer)
		})
	})
}

func TestDeactivatePeer(t *testing.T) {
	t.Parallel()
	t.Run("From Empty", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		err = store.DeactivatePeer(ctx, 4)
		require.ErrorIs(t, err, os.ErrNotExist)
		content, err := os.ReadFile(f.Name())
		require.Nil(t, err)
		require.Equal(t, []byte{}, content)
	})

	t.Run("From Non-Empty", func(t *testing.T) {
		t.Parallel()
		t.Run("Not Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			err = store.DeactivatePeer(ctx, 4)
			require.ErrorIs(t, err, os.ErrNotExist)
		})

		t.Run("Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			err = store.DeactivatePeer(ctx, 0)
			require.Nil(t, err)
		})
	})
}

func TestActivatePeer(t *testing.T) {
	t.Parallel()
	t.Run("From Empty", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		err = store.ActivatePeer(ctx, 4)
		require.ErrorIs(t, err, os.ErrNotExist)
	})

	t.Run("From Non-Empty", func(t *testing.T) {
		t.Parallel()
		t.Run("Not Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			err = store.ActivatePeer(ctx, 4)
			require.ErrorIs(t, err, os.ErrNotExist)
		})

		t.Run("Exists", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)
			err = store.AppendPeer(ctx, db.Peer{
				PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
				PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
				PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
				DateCreated: "2023-04-10T12:08:02+03:30",
				IsActive:    true,
				Name:        "Paul",
				AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
			})
			require.Nil(t, err)
			err = store.ActivatePeer(ctx, 0)
			require.Nil(t, err)
		})
	})
}

type FakeWriter struct {
	writer func([]byte) (int, error)
}

func (wr *FakeWriter) Write(b []byte) (int, error) {
	return wr.writer(b)
}

func TestDumpPeers(t *testing.T) {
	t.Parallel()
	t.Run("No Peers", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		var buf bytes.Buffer
		err = store.DumpPeers(ctx, &buf)
		require.Nil(t, err)
		require.Equal(t, "", buf.String())
	})

	t.Run("With Peers", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		const initialContent = `{
			"settings": {
				"dns": ["1.1.1.1"]
			},
			"peers": [
				{
					"privateKey": "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
					"publicKey": "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
					"psk": "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
					"dateCreated": "2023-04-10T12:02:17+03:30",
					"isActive": true,
					"name": "Jake",
					"allowedIPs": {"ipv4": "10.0.0.1"}
				},
				{
					"privateKey": "cEla2oCjsEajn0W1A8GWf3UVZORspzZAo5IQtXaM1Vk=",
					"publicKey": "paTIcwX30tPuXuvWzSo70NW9m6VwEWdWqgxbKwQdnSI=",
					"psk": "wGov8hmaVzM10nyaFUtCr/J7wiIJwVK2NvS/3pdPlFw=",
					"dateCreated": "2023-04-21T18:05:59+03:30",
					"isActive": false,
					"name": "Defensive",
					"allowedIPs": {"ipv4": "10.0.0.2"}
				},
				{
					"privateKey": "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
					"publicKey": "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
					"psk": "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
					"dateCreated": "2023-04-10T12:08:02+03:30",
					"isActive": true,
					"name": "Paul",
					"allowedIPs": {"ipv4": "10.0.0.3"}
				}
			]
		}`
		n, err := f.WriteString(initialContent)
		require.Nil(t, err)
		require.Equal(t, len(initialContent), n)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		var buf bytes.Buffer
		err = store.DumpPeers(ctx, &buf)
		require.Nil(t, err)
		require.Equal(t,
			`[Peer]
PublicKey    = MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=
PresharedKey = ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=
AllowedIPs   = 10.0.0.1/32
[Peer]
PublicKey    = VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=
PresharedKey = FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=
AllowedIPs   = 10.0.0.3/32
`,
			buf.String())
	})

	t.Run("Write Error", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		const initialContent = `{
			"settings": {
				"dns": ["1.1.1.1"]
			},
			"peers": [
				{
					"privateKey": "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
					"publicKey": "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
					"psk": "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
					"dateCreated": "2023-04-10T12:02:17+03:30",
					"isActive": true,
					"name": "Jake",
					"allowedIPs": {"ipv4": "10.0.0.1/32"}
				},
				{
					"privateKey": "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
					"publicKey": "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
					"psk": "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
					"dateCreated": "2023-04-10T12:08:02+03:30",
					"isActive": true,
					"name": "Paul",
					"allowedIPs": {"ipv4": "10.0.0.2/32"}
				}
			]
		}`
		n, err := f.WriteString(initialContent)
		require.Nil(t, err)
		require.Equal(t, len(initialContent), n)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)

		var i int
		buf := FakeWriter{
			writer: func(b []byte) (int, error) {
				i++
				if i == 2 {
					return 0, errors.New("unknown error")
				}
				return len(b), nil
			},
		}
		err = store.DumpPeers(ctx, &buf)
		require.NotNil(t, err)
	})
}

func TestLastPeerIP(t *testing.T) {
	t.Parallel()
	t.Run("No Peers", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		lastIP, err := store.LastPeerIPs(ctx)
		require.ErrorIs(t, err, os.ErrNotExist)
		require.Nil(t, lastIP)
	})

	t.Run("With Peers", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		const initialContent = `{
			"settings": {
				"dns": ["1.1.1.1"]
			},
			"peers": [
				{
					"privateKey": "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
					"publicKey": "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
					"psk": "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
					"dateCreated": "2023-04-10T12:02:17+03:30",
					"isActive": true,
					"name": "Jake",
					"allowedIPs": {"ipv4": "10.0.0.1/32"}
				},
				{
					"privateKey": "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
					"publicKey": "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
					"psk": "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
					"dateCreated": "2023-04-10T12:08:02+03:30",
					"isActive": true,
					"name": "Paul",
					"allowedIPs": {"ipv4": "10.0.0.2/32"}
				}
			]
		}`
		n, err := f.WriteString(initialContent)
		require.Nil(t, err)
		require.Equal(t, len(initialContent), n)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		lastIP, err := store.LastPeerIPs(ctx)
		require.Nil(t, err)
		require.Equal(t, &db.PeerAllowedIPs{IPv4: "10.0.0.2/32"}, lastIP)
	})
}

func TestPeers(t *testing.T) {
	t.Parallel()
	t.Run("No Peers", func(t *testing.T) {
		t.Parallel()
		f, err := createTemp(t)
		require.Nil(t, err)
		err = f.Close()
		require.Nil(t, err)
		t.Cleanup(func() {
			err := os.Remove(f.Name())
			require.Nil(t, err)
		})

		ctx := context.Background()
		store, err := db.New(ctx, f.Name())
		require.Nil(t, err)
		peers, err := store.Peers(ctx)
		require.ErrorIs(t, err, os.ErrNotExist)
		require.Nil(t, peers)
	})

	t.Run("With 5 Peers", func(t *testing.T) {
		t.Parallel()
		const initialContent = `{
			"settings": {
				"dns": ["1.1.1.1"]
			},
			"peers": [
				{
					"privateKey": "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
					"publicKey": "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
					"psk": "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
					"dateCreated": "2023-04-10T12:02:17+03:30",
					"isActive": true,
					"name": "Jake",
					"allowedIPs": {"ipv4": "10.0.0.1/32"}
				},
				{
					"privateKey": "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
					"publicKey": "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
					"psk": "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
					"dateCreated": "2023-04-10T12:08:02+03:30",
					"isActive": true,
					"name": "Paul",
					"allowedIPs": {"ipv4": "10.0.0.2/32"}
				},
				{
					"privateKey": "MI4euNYLTVqAiaZPa46Q4GYNVwKuxCHGTfXhwyoXTHk=",
					"publicKey": "WL7jOmr05//LXjTvBG7v3ea3ElL+JOJ4CbzjzbfsZn4=",
					"psk": "00UQnqsLfx3htyn+Y3cRI68ou/J68wbQVQsqVt4/k6M=",
					"dateCreated": "2023-04-11T01:59:57+03:30",
					"isActive": true,
					"name": "Stunner",
					"allowedIPs": {"ipv4": "10.0.0.3/32"}
				},
				{
					"privateKey": "kElsKSD9zj0WEYygDD/PYpjhOg9pALrVF0itWMIAYX8=",
					"publicKey": "s96u4K/iOvNtkVb/FWceIhvbnO3VaVU16P0y4GNu/FM=",
					"psk": "mE3udXJvtjayIMESgBQyEBaA+q7eulercZtE8AWDcqI=",
					"dateCreated": "2023-04-11T02:01:25+03:30",
					"isActive": true,
					"name": "Detection",
					"allowedIPs": {"ipv4": "10.0.0.4/32"}
				},
				{
					"privateKey": "2N5NmUDzv29rV/OR9csDX/nZ1BDE2kVJCFp+ZnnGXXM=",
					"publicKey": "iwO+mjtQ7UQq8Ky+tXRrp4rXwA+35l3dU+1LFNAhp0E=",
					"psk": "CyQo7uFWrFJYIulVQWYjIzv+WF7ZDWgQ9ocOPhTEIJ0=",
					"dateCreated": "2023-04-11T02:04:10+03:30",
					"isActive": true,
					"name": "Capillary",
					"allowedIPs": {"ipv4": "10.0.0.5/32"}
				}
			]
		}`
		t.Run("Page 2 - Page Size 20", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(2), db.WithPageSize(20))
			require.ErrorIs(t, err, os.ErrNotExist)
			require.Nil(t, peers)
		})

		t.Run("Page 2 - Page Size 2", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(2), db.WithPageSize(2))
			require.Nil(t, err)
			require.Equal(
				t,
				&db.PeerListResult{
					Total:      5,
					TotalPages: 3,
					Items: []db.PeerListItem{
						{
							ID: 4,
							Peer: db.Peer{
								PrivateKey:  "2N5NmUDzv29rV/OR9csDX/nZ1BDE2kVJCFp+ZnnGXXM=",
								PublicKey:   "iwO+mjtQ7UQq8Ky+tXRrp4rXwA+35l3dU+1LFNAhp0E=",
								PSK:         "CyQo7uFWrFJYIulVQWYjIzv+WF7ZDWgQ9ocOPhTEIJ0=",
								DateCreated: "2023-04-11T02:04:10+03:30",
								IsActive:    true,
								Name:        "Capillary",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.5/32"},
							},
						},
					},
				},
				peers,
			)
		})

		t.Run("Page 1 - Page Size 3", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(1), db.WithPageSize(3))
			require.Nil(t, err)
			require.Equal(
				t,
				&db.PeerListResult{
					Total:      5,
					TotalPages: 2,
					Items: []db.PeerListItem{
						{
							ID: 3,
							Peer: db.Peer{
								PrivateKey:  "kElsKSD9zj0WEYygDD/PYpjhOg9pALrVF0itWMIAYX8=",
								PublicKey:   "s96u4K/iOvNtkVb/FWceIhvbnO3VaVU16P0y4GNu/FM=",
								PSK:         "mE3udXJvtjayIMESgBQyEBaA+q7eulercZtE8AWDcqI=",
								DateCreated: "2023-04-11T02:01:25+03:30",
								IsActive:    true,
								Name:        "Detection",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.4/32"},
							},
						},
						{
							ID: 4,
							Peer: db.Peer{
								PrivateKey:  "2N5NmUDzv29rV/OR9csDX/nZ1BDE2kVJCFp+ZnnGXXM=",
								PublicKey:   "iwO+mjtQ7UQq8Ky+tXRrp4rXwA+35l3dU+1LFNAhp0E=",
								PSK:         "CyQo7uFWrFJYIulVQWYjIzv+WF7ZDWgQ9ocOPhTEIJ0=",
								DateCreated: "2023-04-11T02:04:10+03:30",
								IsActive:    true,
								Name:        "Capillary",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.5/32"},
							},
						},
					},
				},
				peers,
			)
		})

		t.Run("Page 1 - Page Size 5", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(1), db.WithPageSize(5))
			require.ErrorIs(t, err, os.ErrNotExist)
			require.Nil(t, peers)
		})

		t.Run("Page 0 - Page Size 5", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(0), db.WithPageSize(5))
			require.Nil(t, err)
			require.Equal(
				t,
				&db.PeerListResult{
					Total:      5,
					TotalPages: 1,
					Items: []db.PeerListItem{
						{
							ID: 0,
							Peer: db.Peer{
								PrivateKey:  "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
								PublicKey:   "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
								PSK:         "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
								DateCreated: "2023-04-10T12:02:17+03:30",
								IsActive:    true,
								Name:        "Jake",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.1/32"},
							},
						},
						{
							ID: 1,
							Peer: db.Peer{
								PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
								PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
								PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
								DateCreated: "2023-04-10T12:08:02+03:30",
								IsActive:    true,
								Name:        "Paul",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
							},
						},
						{
							ID: 2,
							Peer: db.Peer{
								PrivateKey:  "MI4euNYLTVqAiaZPa46Q4GYNVwKuxCHGTfXhwyoXTHk=",
								PublicKey:   "WL7jOmr05//LXjTvBG7v3ea3ElL+JOJ4CbzjzbfsZn4=",
								PSK:         "00UQnqsLfx3htyn+Y3cRI68ou/J68wbQVQsqVt4/k6M=",
								DateCreated: "2023-04-11T01:59:57+03:30",
								IsActive:    true,
								Name:        "Stunner",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.3/32"},
							},
						},
						{
							ID: 3,
							Peer: db.Peer{
								PrivateKey:  "kElsKSD9zj0WEYygDD/PYpjhOg9pALrVF0itWMIAYX8=",
								PublicKey:   "s96u4K/iOvNtkVb/FWceIhvbnO3VaVU16P0y4GNu/FM=",
								PSK:         "mE3udXJvtjayIMESgBQyEBaA+q7eulercZtE8AWDcqI=",
								DateCreated: "2023-04-11T02:01:25+03:30",
								IsActive:    true,
								Name:        "Detection",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.4/32"},
							},
						},
						{
							ID: 4,
							Peer: db.Peer{
								PrivateKey:  "2N5NmUDzv29rV/OR9csDX/nZ1BDE2kVJCFp+ZnnGXXM=",
								PublicKey:   "iwO+mjtQ7UQq8Ky+tXRrp4rXwA+35l3dU+1LFNAhp0E=",
								PSK:         "CyQo7uFWrFJYIulVQWYjIzv+WF7ZDWgQ9ocOPhTEIJ0=",
								DateCreated: "2023-04-11T02:04:10+03:30",
								IsActive:    true,
								Name:        "Capillary",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.5/32"},
							},
						},
					},
				},
				peers,
			)
		})

		t.Run("Page 0 - Page Size 10", func(t *testing.T) {
			t.Parallel()
			f, err := createTemp(t)
			require.Nil(t, err)

			n, err := f.WriteString(initialContent)
			require.Nil(t, err)
			require.Equal(t, len(initialContent), n)
			err = f.Close()
			require.Nil(t, err)
			t.Cleanup(func() {
				err := os.Remove(f.Name())
				require.Nil(t, err)
			})

			ctx := context.Background()
			store, err := db.New(ctx, f.Name())
			require.Nil(t, err)

			peers, err := store.Peers(ctx, db.WithPageNo(0), db.WithPageSize(10))
			require.Nil(t, err)
			require.Equal(
				t,
				&db.PeerListResult{
					Total:      5,
					TotalPages: 1,
					Items: []db.PeerListItem{
						{
							ID: 0,
							Peer: db.Peer{
								PrivateKey:  "mOGOaug8v9Wpk2ZghJheudILupqlb3UgATb2OROAUHc=",
								PublicKey:   "MxOYIKQo5bgxF3DL5jrYeVoPn8KGRkIwBqWPfxZSpUQ=",
								PSK:         "ph3VyIwdFa9o9rJjlOJi7uGLQZcp7Psab6FqUW+0unY=",
								DateCreated: "2023-04-10T12:02:17+03:30",
								IsActive:    true,
								Name:        "Jake",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.1/32"},
							},
						},
						{
							ID: 1,
							Peer: db.Peer{
								PrivateKey:  "+PcQScoY2OVgEMf5h/RyqWSnP3TvbUvmoHzyGC6jJWo=",
								PublicKey:   "VBsF7b8bAml5ICItIDFOypDrO/Ja92I5JzmxJ8bp2CY=",
								PSK:         "FXilz7WdYeniafVYf6SVDTurIDrDQTIEKOoyd2210ss=",
								DateCreated: "2023-04-10T12:08:02+03:30",
								IsActive:    true,
								Name:        "Paul",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.2/32"},
							},
						},
						{
							ID: 2,
							Peer: db.Peer{
								PrivateKey:  "MI4euNYLTVqAiaZPa46Q4GYNVwKuxCHGTfXhwyoXTHk=",
								PublicKey:   "WL7jOmr05//LXjTvBG7v3ea3ElL+JOJ4CbzjzbfsZn4=",
								PSK:         "00UQnqsLfx3htyn+Y3cRI68ou/J68wbQVQsqVt4/k6M=",
								DateCreated: "2023-04-11T01:59:57+03:30",
								IsActive:    true,
								Name:        "Stunner",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.3/32"},
							},
						},
						{
							ID: 3,
							Peer: db.Peer{
								PrivateKey:  "kElsKSD9zj0WEYygDD/PYpjhOg9pALrVF0itWMIAYX8=",
								PublicKey:   "s96u4K/iOvNtkVb/FWceIhvbnO3VaVU16P0y4GNu/FM=",
								PSK:         "mE3udXJvtjayIMESgBQyEBaA+q7eulercZtE8AWDcqI=",
								DateCreated: "2023-04-11T02:01:25+03:30",
								IsActive:    true,
								Name:        "Detection",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.4/32"},
							},
						},
						{
							ID: 4,
							Peer: db.Peer{
								PrivateKey:  "2N5NmUDzv29rV/OR9csDX/nZ1BDE2kVJCFp+ZnnGXXM=",
								PublicKey:   "iwO+mjtQ7UQq8Ky+tXRrp4rXwA+35l3dU+1LFNAhp0E=",
								PSK:         "CyQo7uFWrFJYIulVQWYjIzv+WF7ZDWgQ9ocOPhTEIJ0=",
								DateCreated: "2023-04-11T02:04:10+03:30",
								IsActive:    true,
								Name:        "Capillary",
								AllowedIPs:  db.PeerAllowedIPs{IPv4: "10.0.0.5/32"},
							},
						},
					},
				},
				peers,
			)
		})
	})
}
