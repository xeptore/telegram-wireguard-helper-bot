# WireGuard Helper Telegram Bot

## Setup

1. Install WireGuard if you haven't already

    ```sh
    sudo apt update && sudo apt install -y wireguard wireguard-tools
    ```

2. Create WireGuard server config:

    ```ini
    [Interface]
    Address = 10.0.0.1/16
    ListenPort = port
    MTU = 1280
    PrivateKey = server_private_key
    PostUp = wg addconf %i <(tgwg --db path_to_db_file dump)
    ```

3. Download latest released executable binary

    ```sh
    curl -SfLO https://gitlab.com/api/v4/projects/xeptore%2Ftelegram-wireguard-helper-bot/packages/generic/tgwg/latest/tgwg && chmod +x ./tgwg
    ```

4. Set environment variables

    ```sh
    curl -SfL --output .env https://gitlab.com/xeptore/telegram-wireguard-helper-bot/-/raw/main/.env.template
    ```

    Set proper values in `.env`. For example:

    ```env
    BOT_HTTP_PROXY_URL=socks5://127.0.0.1:1080 # Note that the schema (socks5, socks4, http) is required
    ALLOWED_USER_IDS=641362959 # ID of the user allowed to interact with the bot
    BOT_TOKEN=6206664886:AAGjV2HHXR6pSWmBH6ifmFXiS-ehvty4068 # Token Bot Father gave you
    ```

5. Create SystemD service

    ```sh
    sudo cat >/etc/systemd/system/tgwg.service <<EOF
    [Unit]
    Description=WireGuard Helper Telegram Bot
    After=network.target
    Wants=network-online.target

    [Service]
    Restart=on-failure
    Type=simple
    ExecStart=tgwg --db path_to_db_file bot --addr your_domain --conf path_to_server_conf
    EnvironmentFile=path_to_dotenv_file
    RestartSec=10s
    TimeoutStopSec=20s
    KillSignal=SIGINT
    FinalKillSignal=SIGKILL

    [Install]
    WantedBy=multi-user.target
    EOF
    ```

6. Restart and enable the service to start after boot

    ```sh
    sudo systemctl daemon-reload
    sudo systemctl start tgwg.service
    sudo systemctl enable tgwg.service
    ```

## Update

1. Stop the service

    ```sh
    sudo systemctl stop tgwg.service
    ```

2. Download latest released executable binary

    ```sh
    curl -SfLO https://gitlab.com/api/v4/projects/xeptore%2Ftelegram-wireguard-helper-bot/packages/generic/tgwg/latest/tgwg && chmod +x ./tgwg
    ```

3. Restart the service

    ```sh
    sudo systemctl restart tgwg.service
    ```

4. Send `/start` command to the bot to restart it
