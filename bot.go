package main

import (
	"errors"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/go-telegram/bot"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/urfave/cli/v2"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"

	"gitlab.com/xeptore/telegram-wireguard-helper/db"
)

func buildBot(log zerolog.Logger) func(*cli.Context) error {
	return func(cliCtx *cli.Context) error {
		ctx, cancel := signal.NotifyContext(cliCtx.Context, os.Interrupt)
		defer cancel()

		if err := godotenv.Load(); nil != err {
			if !errors.Is(err, os.ErrNotExist) {
				log.Fatal().Err(err).Msg("unexpected error while loading .env file")
			}
			log.Warn().Msg(".env file not found")
		}

		loadAllowedUserIDs(log)

		wg, err := wgctrl.New()
		if nil != err {
			log.Fatal().Err(err).Msg("failed to initialize WireGuard controller client")
		}

		store, err := db.New(ctx, cliCtx.String(CLIFlagDB))
		if nil != err {
			log.Fatal().Err(err).Msg("failed to initialize database")
		}

		handler := Handler{
			logger:       log.With().Str("app", "handler").Logger(),
			wg:           wg,
			store:        store,
			ServerAddr:   cliCtx.String(CLICommandBotFlagAddr),
			ConfFilePath: cliCtx.String(CLICommandBotFlagConf),
		}

		activePeers, err := store.WGActivePeers(ctx)
		if nil != err {
			log.Fatal().Err(err).Msg("failed to retrieve list of active peers for WireGuard device initial sync")
		}
		err = wg.ConfigureDevice(handler.wgDeviceName(), wgtypes.Config{
			ReplacePeers: false,
			Peers:        activePeers,
		})
		if nil != err && !errors.Is(err, os.ErrNotExist) {
			log.Fatal().Err(err).Msg("failed to configure WireGuard device with active peers")
		}

		httpTransport := http.Transport{IdleConnTimeout: 10 * time.Second, ResponseHeaderTimeout: 30 * time.Second}
		httpClient := http.Client{Timeout: time.Second * 35, Transport: &httpTransport}
		proxyURL, ok := os.LookupEnv(BotHTTPProxyURL)
		if ok {
			httpProxyURL, err := url.Parse(proxyURL)
			if nil != err {
				log.Fatal().Err(err).Msg("failed to parse bot http proxy url")
			}
			httpTransport.Proxy = http.ProxyURL(httpProxyURL)
		}

		opts := []bot.Option{
			bot.WithCheckInitTimeout(30 * time.Second),
			bot.WithHTTPClient(30*time.Second, &httpClient),
		}
		token, ok := os.LookupEnv(BotTokenEnvKey)
		if !ok {
			log.Fatal().Str("key", BotTokenEnvKey).Msg("required environment variable is not set")
		}
		b, err := bot.New(token, opts...)
		if nil != err {
			log.Fatal().Err(err).Msg("failed to initialize bot instance")
		}
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandStart, bot.MatchTypeExact, handler.handleStartCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandCreate, bot.MatchTypePrefix, handler.handleCreateCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandDeactivate, bot.MatchTypePrefix, handler.handleDeactivateCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandActivate, bot.MatchTypePrefix, handler.handleActivateCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandList, bot.MatchTypePrefix, handler.handleListCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandGetPeer, bot.MatchTypePrefix, handler.handleGetPeerCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandSetDNS, bot.MatchTypePrefix, handler.handleSetDNSCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandRenamePeer, bot.MatchTypePrefix, handler.handleRenamePeerCommand)
		b.RegisterHandler(bot.HandlerTypeMessageText, CommandRotateKeys, bot.MatchTypePrefix, handler.handleSRotateKeysCommand)
		b.RegisterHandler(bot.HandlerTypeCallbackQueryData, CallbackMatchPrefixPeersList, bot.MatchTypePrefix, handler.handleListCallback)
		b.Start(ctx)

		return nil
	}
}
