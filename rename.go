package main

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/rs/zerolog"
	"github.com/xeptore/wireuse/pkg/funcutils"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
)

func sendHelp(ctx context.Context, b *bot.Bot, logger *zerolog.Logger, chatID int64, text string) {
	_, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:    chatID,
		Text:      text,
		ParseMode: ParseModeMarkdownV1,
	})
	if nil != err {
		logger.Error().Err(err).Msg("failed to send command help message")
	}
}

func (h *Handler) handleRenamePeerCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID

	sendHelp := func() {
		sendHelp(
			ctx,
			b,
			&h.logger,
			chatID,
			fmt.Sprintf("Use `%s peer_index new_name` message format.", CommandRenamePeer),
		)
	}

	arg := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandRenamePeer))
	parts := funcutils.Map(strings.SplitN(arg, " ", 2), func(p string) string { return strings.TrimSpace(p) })
	if len(parts) != 2 || len(parts[0]) == 0 || len(parts[1]) == 0 {
		sendHelp()
		return
	}
	peerIndex, err := strconv.Atoi(parts[0])
	if nil != err || peerIndex < 0 {
		sendHelp()
		return
	}

	newName := parts[1]
	previousName, err := h.store.RenamePeer(ctx, peerIndex, newName)
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to persist peer rename into store")
		if _, err = b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		}); nil != err {
			h.logger.Error().Err(err).Msg("failed to send failure error reply message")
			return
		}
		return
	}
	_, err = b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("✅ Renamed from `%s` to `%s`.", previousName, newName),
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send success reply message")
		return
	}
}
