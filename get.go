package main

import (
	"bytes"
	"context"
	"fmt"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"github.com/skip2/go-qrcode"
)

func (h *Handler) sendPeerConf(ctx context.Context, b *bot.Bot, update *models.Update, confFileContent []byte, peerIsActive bool) {
	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	var confMsgTextContentPrefix string
	if peerIsActive {
		confMsgTextContentPrefix = "✅ Peer is active"
	} else {
		confMsgTextContentPrefix = "⚠️ Peer is not active"
	}
	_, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("%s\n\n```\n%s\n```", confMsgTextContentPrefix, bot.EscapeMarkdown(string(bytes.TrimSpace(confFileContent)))),
		ParseMode:        models.ParseModeMarkdown,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send peer config file content")

		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to send peer config file content 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	_, err = b.SendDocument(ctx, &bot.SendDocumentParams{
		ChatID:           chatID,
		Document:         &models.InputFileUpload{Filename: "wg0.conf", Data: bytes.NewBuffer(confFileContent)},
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload peer config file")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to upload peer config file 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	png, err := qrcode.Encode(string(bytes.Clone(confFileContent)), qrcode.High, 1024)
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload peer config qr code image")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to generate peer config QR code image 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	_, err = b.SendPhoto(ctx, &bot.SendPhotoParams{
		ChatID:           chatID,
		Photo:            &models.InputFileUpload{Filename: "wg0.png", Data: bytes.NewBuffer(png)},
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload peer config qr code image")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to upload peer config QR code image 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}
}
