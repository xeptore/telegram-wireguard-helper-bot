package main

import (
	"bytes"
	"context"
	_ "embed"
	"errors"
	"fmt"
	"net"
	"net/netip"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"github.com/rs/zerolog"
	"github.com/skip2/go-qrcode"
	"github.com/urfave/cli/v2"
	"github.com/xeptore/wireuse/pkg/funcutils"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/ini.v1"

	"gitlab.com/xeptore/telegram-wireguard-helper/db"
	"gitlab.com/xeptore/telegram-wireguard-helper/db/upgrade"
	"gitlab.com/xeptore/telegram-wireguard-helper/table"
)

const (
	AllowedUserIDsEnvKey         = "ALLOWED_USER_IDS"
	BotTokenEnvKey               = "BOT_TOKEN"
	BotHTTPProxyURL              = "BOT_HTTP_PROXY_URL"
	ParseModeMarkdownV1          = models.ParseMode("Markdown")
	CommandDeactivate            = "/deactivate"
	CommandActivate              = "/activate"
	CommandList                  = "/list"
	CommandCreate                = "/create"
	CommandRotateKeys            = "/rotate"
	CommandStart                 = "/start"
	CommandGetPeer               = "/get"
	CommandSetDNS                = "/setdns"
	CommandRenamePeer            = "/rename"
	CallbackMatchPrefixPeersList = "peers_list_callback_match_"
	ListCallbackNoopParam        = "noop"
	CLICommandBotName            = "bot"
	CLICommandDumpName           = "dump"
	CLICommandBotFlagAddr        = "addr"
	CLIFlagDB                    = "db"
	CLICommandBotFlagConf        = "conf"
)

var (
	allowedUserIDs []string
	AppName        = ""
	AppVersion     = ""
	AppCompileTime = ""
)

//go:embed wg.png
var wgPNG []byte

func main() {
	compileTime, err := time.Parse(time.RFC3339, AppCompileTime)
	if nil != err {
		panic(err)
	}

	log := zerolog.New(zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) { w.Out = os.Stderr; w.TimeFormat = time.RFC3339 })).With().Timestamp().Logger().Level(zerolog.TraceLevel)

	app := &cli.App{
		Name:           AppName,
		Version:        AppVersion,
		Compiled:       compileTime,
		Suggest:        true,
		Usage:          "The WireGuard Helper Telegram Bot",
		DefaultCommand: CLICommandBotName,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     CLIFlagDB,
				Aliases:  []string{"f"},
				Usage:    "Peers database file path",
				Required: true,
			},
		},
		Before: func(ctx *cli.Context) error {
			log.Debug().Msg("upgrading database")
			if err := upgrade.Run(ctx.Context, log, ctx.String(CLIFlagDB)); nil != err {
				if errors.Is(err, upgrade.ErrAlreadyLatest) {
					log.Info().Msg("database is already at the latest version")
					return nil
				}
				if errors.Is(err, upgrade.ErrUnknownVersion) {
					log.Error().Msg("could not detect current database version")
					return nil
				}

				log.Error().Err(err).Msg("failed to upgrade database")
				return err
			}
			return nil
		},
		Commands: []*cli.Command{
			{
				Name:    CLICommandBotName,
				Usage:   "Starts bot server",
				Aliases: []string{"b"},
				Action:  buildBot(log),
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     CLICommandBotFlagAddr,
						Aliases:  []string{"a"},
						Usage:    "Server public hostname",
						Required: true,
					},
					&cli.StringFlag{
						Name:     CLICommandBotFlagConf,
						Aliases:  []string{"c"},
						Usage:    "Server conf file path",
						Required: true,
					},
				},
			},
			{
				Name:    CLICommandDumpName,
				Usage:   "Prints out all peers in the WireGuard-compatible format",
				Aliases: []string{"d"},
				Action:  buildDump(log),
				Flags:   []cli.Flag{},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal().Err(err).Msg("command failed")
	}
}

func (h *Handler) handleStartCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	if _, err := b.SetMyName(ctx, &bot.SetMyNameParams{
		Name: "WireGuard Helper",
	}); nil != err {
		h.logger.Error().Err(err).Msg("failed to set bot name")
	}

	if _, err := b.SetMyCommands(ctx, &bot.SetMyCommandsParams{
		Scope: &models.BotCommandScopeAllPrivateChats{},
		Commands: []models.BotCommand{
			{
				Command:     CommandStart,
				Description: "Restart Me",
			},
			{
				Command:     CommandSetDNS,
				Description: "Set Peers DNS Option",
			},
			{
				Command:     CommandRenamePeer,
				Description: "Rename Peer",
			},
			{
				Command:     CommandRotateKeys,
				Description: "Rotate A Peer Keys",
			},
			{
				Command:     CommandCreate,
				Description: "Create Peer",
			},
			{
				Command:     CommandList,
				Description: "List Peers",
			},
			{
				Command:     CommandDeactivate,
				Description: "Deactivate Peer",
			},
			{
				Command:     CommandActivate,
				Description: "Activate Peer",
			},
			{
				Command:     CommandGetPeer,
				Description: "Get Peer Config Files",
			},
		},
	}); nil != err {
		h.logger.Error().Err(err).Msg("failed to set bot commands")
	}

	if _, err := b.SendPhoto(ctx, &bot.SendPhotoParams{
		ChatID:    update.Message.Chat.ID,
		Caption:   "_I wish I could also set this photo as my profile photo, but currently, Telegram does not allow me to do so_ 😥\n_However, you can do this going to @BotFather, sending /setuserpic command, selecting my username, then sharing this photo with @BotFather_ 🥲",
		Photo:     &models.InputFileUpload{Filename: "profile.png", Data: bytes.NewBuffer(wgPNG)},
		ParseMode: models.ParseModeMarkdown,
	}); nil != err {
		h.logger.Error().Err(err).Msg("failed to send bot profile photo message")
	}

	if _, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID: update.Message.Chat.ID,
		Text: strings.Join(
			[]string{
				fmt.Sprintf("*%s*", AppName),
				fmt.Sprintf("Compiled At: `%s`", bot.EscapeMarkdown(AppCompileTime)),
				fmt.Sprintf("Version: `%s`", bot.EscapeMarkdown(AppVersion)),
			},
			"\n",
		),
		ParseMode: models.ParseModeMarkdown,
	}); nil != err {
		h.logger.Error().Err(err).Msg("failed to send start command success reply message")
	}
}

func (h *Handler) handleCreateCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandCreate))
	if len(strings.TrimSpace(args)) == 0 {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s name_of_the_peer` message format.", CommandCreate),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	}
	h.logger.Debug().Str("args", args).Msg("processing create peer request")

	loadingMessage, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("Creating a new peer for *%s*. Please wait...", bot.EscapeMarkdown(args)),
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send loading message to the user")
	}

	confFileContent, err := h.createPeer(ctx, args)
	if nil != err && !errors.Is(err, errWGDevicePeerAdd) {
		h.logger.Error().Err(err).Msg("failed to create config")
		if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
			h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
		}

		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("Failed to create the request config 😞\n\nError:\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}
	if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
		h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
	}

	_, err = b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("```\n%s\n```", bot.EscapeMarkdown(string(bytes.TrimSpace(confFileContent)))),
		ParseMode:        models.ParseModeMarkdown,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send peer config file content")

		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to send peer config file content 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	_, err = b.SendDocument(ctx, &bot.SendDocumentParams{
		ChatID:           chatID,
		Document:         &models.InputFileUpload{Filename: "wg0.conf", Data: bytes.NewBuffer(confFileContent)},
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload peer config file")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to upload peer config file 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	png, err := qrcode.Encode(string(bytes.Clone(confFileContent)), qrcode.High, 1024)
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload created peer config qr code image")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to generate peer config QR code image 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	_, err = b.SendPhoto(ctx, &bot.SendPhotoParams{
		ChatID:           chatID,
		Photo:            &models.InputFileUpload{Filename: "wg0.png", Data: bytes.NewBuffer(png)},
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to upload peer config qr code image")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "Failed to upload peer config QR code image 😞",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	if errors.Is(err, errWGDevicePeerAdd) {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("Failed to add new peer to WireGuard device ⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send WireGuard device change operation failure message")
			return
		}
	}
}

func (h *Handler) handleDeactivateCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandDeactivate))
	index, err := strconv.Atoi(args)
	if len(strings.TrimSpace(args)) == 0 || nil != err {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s peer_id` message format.", CommandDeactivate),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	}
	h.logger.Debug().Str("args", args).Msg("processing deactivate peer request")

	loadingMessage, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("Deactivating peer *%s*. Please wait...", bot.EscapeMarkdown(args)),
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send loading reply message to the user")
	}

	err = h.deactivatePeer(ctx, index)
	if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
		h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
	}
	if nil == err || errors.Is(err, os.ErrNotExist) {
		h.logger.Info().Str("name", args).Msg("successfully deactivated peer")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "✅",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send success reply message")
			return
		}
		return
	}
	h.logger.Error().Err(err).Msg("failed to deactivate peer")

	_, err = b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
		ParseMode:        models.ParseModeMarkdown,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send failure error reply message")
		return
	}
}

func (h *Handler) handleActivateCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandActivate))
	index, err := strconv.Atoi(args)
	if len(strings.TrimSpace(args)) == 0 || nil != err {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s peer_id` message format.", CommandActivate),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	}

	loadingMessage, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("Activating peer *%s*. Please wait...", bot.EscapeMarkdown(args)),
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send loading reply message to the user")
	}

	err = h.activatePeer(ctx, index)
	if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
		h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
	}
	if nil == err || errors.Is(err, os.ErrNotExist) {
		h.logger.Info().Str("name", args).Msg("successfully activated peer")
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             "✅",
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send success reply message")
			return
		}
		return
	}
	h.logger.Error().Err(err).Msg("failed to activate peer")

	_, err = b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("⚠️\n\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
		ParseMode:        models.ParseModeMarkdown,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send failure error reply message")
		return
	}
}

func (h *Handler) handleGetPeerCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID
	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandGetPeer))
	id, err := strconv.Atoi(args)
	if len(strings.TrimSpace(args)) == 0 || nil != err {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s peer_id` message format.", CommandGetPeer),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	}

	loadingMessage, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             fmt.Sprintf("Loading peer *%s* config. Please wait...", bot.EscapeMarkdown(args)),
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send loading reply message to the user")
	}

	confFileContent, peerIsActive, err := h.getPeer(ctx, id)
	if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
		h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
	}
	if nil != err {
		if errors.Is(err, os.ErrNotExist) {
			_, err := b.SendMessage(ctx, &bot.SendMessageParams{
				ChatID:           chatID,
				Text:             "Peer was not found.",
				ParseMode:        models.ParseModeMarkdown,
				ReplyToMessageID: msgID,
			})
			if nil != err {
				h.logger.Error().Err(err).Msg("failed to send reply message")
			}
			return
		}
		h.logger.Error().Err(err).Int("peerID", id).Msg("failed to get peer config")

		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:           chatID,
			Text:             fmt.Sprintf("Failed to get peer config 😞\n\nError:\n```\n%s\n```", bot.EscapeMarkdown(err.Error())),
			ParseMode:        models.ParseModeMarkdown,
			ReplyToMessageID: msgID,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send error acknowledge message")
		}
		return
	}

	h.sendPeerConf(ctx, b, update, confFileContent, peerIsActive)
}

func (h *Handler) handleListCallback(ctx context.Context, b *bot.Bot, update *models.Update) {
	ok, err := b.AnswerCallbackQuery(ctx, &bot.AnswerCallbackQueryParams{
		CallbackQueryID: update.CallbackQuery.ID,
		ShowAlert:       false,
	})
	if nil != err {
		h.logger.Error().Err(err).Str("CallbackQueryID", update.CallbackQuery.ID).Msg("failed to answer callback query")
	}
	if !ok {
		h.logger.Warn().Str("CallbackQueryID", update.CallbackQuery.ID).Msg("received not-ok response from answering callback query")
	}
	args := strings.TrimPrefix(update.CallbackQuery.Data, CallbackMatchPrefixPeersList)
	if args == ListCallbackNoopParam {
		return
	}

	pageNo, err := strconv.Atoi(args)
	if nil != err || pageNo < 0 {
		return
	}

	list, err := h.listPeers(ctx, pageNo)
	// TODO: handle err
	var text string
	if list.Total == 0 {
		text = fmt.Sprintf("*No Peers*\n\n_Page: %d_\n_Pages: %d_\n_Update: %s_", pageNo, list.TotalPages, time.Now().Format("15:04:05"))
	} else {
		t := table.Render(funcutils.Map(list.Peers, func(p PeersListItem) table.PeerRow {
			return table.PeerRow{
				ID:            p.ID,
				Name:          p.Name,
				PublicKey:     p.PublicKey[:20] + "...",
				AllowedIPs:    p.AllowedIPs,
				UploadBytes:   p.UploadBytes,
				DownloadBytes: p.DownloadBytes,
				IsActive:      p.IsActive,
			}
		}))
		text = fmt.Sprintf("```\n%s\n```\n_Page: %d_\n_Pages: %d_\n_Update: %s_", string(t), pageNo, list.TotalPages, time.Now().Format("15:04:05"))
	}
	_, err = b.EditMessageText(ctx, &bot.EditMessageTextParams{
		ChatID:          update.CallbackQuery.Message.Chat.ID,
		MessageID:       update.CallbackQuery.Message.ID,
		InlineMessageID: update.CallbackQuery.InlineMessageID,
		Text:            text,
		ParseMode:       ParseModeMarkdownV1,
		ReplyMarkup:     buildKeyboard(pageNo, list.TotalPages),
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to update list message with new page content")
		return
	}
}

func buildKeyboard(pageNo, totalPages int) models.ReplyMarkup {
	rowButtons := make([]models.InlineKeyboardButton, 0, 4)

	if pageNo > 0 {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "⏪", CallbackData: CallbackMatchPrefixPeersList + "0"})
	} else {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "⏪", CallbackData: CallbackMatchPrefixPeersList + ListCallbackNoopParam})
	}

	if pageNo > 0 {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "◀️", CallbackData: CallbackMatchPrefixPeersList + strconv.Itoa(pageNo-1)})
	} else {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "◀️", CallbackData: CallbackMatchPrefixPeersList + ListCallbackNoopParam})
	}

	if pageNo < totalPages-1 {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "▶️", CallbackData: CallbackMatchPrefixPeersList + strconv.Itoa(pageNo+1)})
	} else {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "▶️", CallbackData: CallbackMatchPrefixPeersList + ListCallbackNoopParam})
	}

	if pageNo < totalPages-1 {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "⏩", CallbackData: CallbackMatchPrefixPeersList + strconv.Itoa(totalPages-1)})
	} else {
		rowButtons = append(rowButtons, models.InlineKeyboardButton{Text: "⏩", CallbackData: CallbackMatchPrefixPeersList + ListCallbackNoopParam})
	}

	reloadButton := models.InlineKeyboardButton{Text: "🔄", CallbackData: CallbackMatchPrefixPeersList + strconv.Itoa(pageNo)}

	return models.InlineKeyboardMarkup{
		InlineKeyboard: [][]models.InlineKeyboardButton{
			rowButtons,
			{reloadButton},
		},
	}
}

func (h *Handler) handleListCommand(ctx context.Context, b *bot.Bot, update *models.Update) {
	if update.Message == nil {
		return
	}
	if !isFromAllowedUser(update.Message.From.ID) {
		return
	}

	chatID := update.Message.Chat.ID
	msgID := update.Message.ID

	args := strings.TrimSpace(strings.TrimPrefix(update.Message.Text, CommandList))
	pageNo, err := strconv.Atoi(args)
	if len(args) != 0 && nil != err || pageNo < 0 {
		_, err := b.SendMessage(ctx, &bot.SendMessageParams{
			ChatID:    chatID,
			Text:      fmt.Sprintf("Use `%s page_no` message format.", CommandList),
			ParseMode: ParseModeMarkdownV1,
		})
		if nil != err {
			h.logger.Error().Err(err).Msg("failed to send command help message")
		}
		return
	} else if len(args) == 0 {
		pageNo = 0
	}

	loadingMessage, err := b.SendMessage(ctx, &bot.SendMessageParams{
		ChatID:           chatID,
		Text:             "Listing peers. Please wait...",
		ParseMode:        ParseModeMarkdownV1,
		ReplyToMessageID: msgID,
	})
	if nil != err {
		h.logger.Error().Err(err).Msg("failed to send loading reply message to the user")
	}

	list, err := h.listPeers(ctx, pageNo)
	if _, err := b.DeleteMessage(ctx, &bot.DeleteMessageParams{ChatID: chatID, MessageID: loadingMessage.ID}); nil != err {
		h.logger.Error().Err(err).Int("previousLoadingMessageId", loadingMessage.ID).Msg("failed to delete previous loading message")
	}
	if nil != err {
		h.logger.Error().Err(err).Int64("chatID", chatID).Int("pageNo", pageNo).Msg("failed to get list of peers")
		return
	}
	msg := bot.SendMessageParams{
		ChatID:    chatID,
		ParseMode: ParseModeMarkdownV1,
	}
	if list.Total == 0 {
		msg.Text = fmt.Sprintf("*No Peers*\n\n_Page: %d_\n_Pages: %d_\n_Update: %s_", pageNo, list.TotalPages, time.Now().Format("15:04:05"))
		msg.ReplyMarkup = buildKeyboard(pageNo, list.TotalPages)
	} else {
		text := table.Render(funcutils.Map(list.Peers, func(p PeersListItem) table.PeerRow {
			return table.PeerRow{
				ID:            p.ID,
				Name:          p.Name,
				PublicKey:     p.PublicKey[:20] + "...",
				AllowedIPs:    p.AllowedIPs,
				UploadBytes:   p.UploadBytes,
				DownloadBytes: p.DownloadBytes,
				IsActive:      p.IsActive,
			}
		}))
		msg.Text = fmt.Sprintf("```\n%s\n```\n_Page: %d_\n_Pages: %d_\n_Update: %s_", string(text), pageNo, list.TotalPages, time.Now().Format("15:04:05"))
		msg.ReplyMarkup = buildKeyboard(pageNo, list.TotalPages)
	}
	if _, err := b.SendMessage(ctx, &msg); nil != err {
		h.logger.Error().Err(err).Msg("failed to send reply message")
		return
	}
}

func loadAllowedUserIDs(log zerolog.Logger) {
	v, ok := os.LookupEnv(AllowedUserIDsEnvKey)
	if !ok {
		log.Fatal().Str("key", AllowedUserIDsEnvKey).Msg("required environment variable is not set")
	}
	parts := strings.Split(v, ",")
	allowedUserIDs = funcutils.Map(parts, func(p string) string {
		return strings.TrimSpace(p)
	})
}

func isFromAllowedUser(uid int64) bool {
	userID := fmt.Sprintf("%d", uid)
	for _, v := range allowedUserIDs {
		if userID == v {
			return true
		}
	}

	return false
}

var (
	ErrInvalidIPv4Address = errors.New("invalid ipv4 address")
	ErrNoMoreIPv4Address  = errors.New("no more ipv4 address is available in the range")
)

func findNextIpv4(srcIPv4 string, serverIPv4Net *net.IPNet) (string, error) {
	ipAddr, err := netip.ParseAddr(srcIPv4)
	if nil != err {
		return "", fmt.Errorf("unable to parse source IPv4 address: %v", err)
	}
	if !ipAddr.Is4() || !ipAddr.IsValid() || !ipAddr.IsPrivate() || ipAddr.IsLoopback() || ipAddr.IsMulticast() || ipAddr.IsUnspecified() {
		return "", fmt.Errorf("invalid source IPv4 address: %w", ErrInvalidIPv4Address)
	}

	nextIP := ipAddr.Next()
	if !nextIP.IsValid() || !nextIP.IsPrivate() || nextIP.IsUnspecified() || !serverIPv4Net.Contains(nextIP.AsSlice()) {
		return "", ErrNoMoreIPv4Address
	}

	nextIPstr := nextIP.String()
	if nextIPstr == "invalid IP" {
		return "", ErrNoMoreIPv4Address
	}

	return nextIPstr, nil
}

type DeviceConf struct {
	Address struct {
		IPv4CIDR string
	}
	PublicKey  string
	ListenPort int
}

type PeersConf []PeerConf

type PeerConf struct {
	AllowedIPs PeerConfAllowedIPs
}
type PeerConfAllowedIPs struct {
	IPv4 string
}

func parseDeviceConfFile(raw []byte) (*DeviceConf, error) {
	file, err := ini.LoadSources(ini.LoadOptions{AllowNonUniqueSections: true}, raw)
	if nil != err {
		return nil, err
	}
	out := new(DeviceConf)
	ifaceSection := file.Section("Interface")
	out.Address.IPv4CIDR = ifaceSection.Key("Address").String()

	out.ListenPort, err = ifaceSection.Key("ListenPort").Int()
	if nil != err {
		return nil, err
	}

	key := ifaceSection.Key("PrivateKey")
	wgKey, err := wgtypes.ParseKey(strings.TrimSpace(key.String()))
	if nil != err {
		return nil, err
	}
	out.PublicKey = wgKey.PublicKey().String()

	return out, nil
}

type Handler struct {
	logger            zerolog.Logger
	wg                *wgctrl.Client
	store             *db.DB
	ServerAddr        string
	ConfFilePath      string
	PeersConfFilePath string
}

var (
	errWGDevicePeerAdd = errors.New("unable to add peer to WireGuard device")
)

type PeerServerConfTemplateParams struct {
	Name         string
	GeneratedAt  string
	PublicKey    string
	PresharedKey string
	AllowedIPs   string
}

func (h *Handler) createPeer(ctx context.Context, name string) ([]byte, error) {
	lastIP, err := h.store.LastPeerIPs(ctx)
	if nil != err && !errors.Is(err, os.ErrNotExist) {
		return nil, fmt.Errorf("unexpected error while finding last used ip addresses: %v", err)
	}

	confFileContent, err := os.ReadFile(h.ConfFilePath)
	if nil != err {
		return nil, fmt.Errorf("failed to read .conf file: %v", err)
	}
	devConf, err := parseDeviceConfFile(confFileContent)
	if nil != err {
		return nil, fmt.Errorf("failed to parse device .conf file: %v", err)
	}

	serverIPv4, serverIPv4Net, err := net.ParseCIDR(devConf.Address.IPv4CIDR)
	if nil != err {
		return nil, fmt.Errorf("failed to parse server ipv4 cidr address: %v", err)
	}
	lastIPv4 := ""
	if lastIP != nil {
		lastIPv4 = lastIP.IPv4
	} else {
		lastIPv4 = serverIPv4.String()
	}

	ipv4, err := findNextIpv4(lastIPv4, serverIPv4Net)
	if nil != err {
		return nil, fmt.Errorf("failed to find next ipv4 address: %v", err)
	}

	key, err := wgtypes.GeneratePrivateKey()
	if nil != err {
		return nil, fmt.Errorf("failed to generate peer private key: %v", err)
	}

	presharedKey, err := wgtypes.GenerateKey()
	if nil != err {
		return nil, fmt.Errorf("failed to generate peer preshared key: %v", err)
	}

	generatedAt := time.Now().Format(time.RFC3339)

	if err := h.store.AppendPeer(ctx, db.Peer{
		PrivateKey:  key.String(),
		PublicKey:   key.PublicKey().String(),
		PSK:         presharedKey.String(),
		DateCreated: generatedAt,
		IsActive:    true,
		Name:        name,
		AllowedIPs:  db.PeerAllowedIPs{IPv4: ipv4},
	}); nil != err {
		return nil, fmt.Errorf("failed to persist new peer info: %v", err)
	}

	var buf bytes.Buffer
	err = template.Must(template.New("").Parse(`# {{.GeneratedAt}}
# {{.Name}}
[Interface]
PrivateKey = {{.PrivateKey}}
Address = {{.IPv4}}/32
DNS = {{.PeerDns}}

[Peer]
PublicKey = {{.ServerPublicKey}}
PresharedKey = {{.PresharedKey}}
Endpoint = {{.ServerEndpoint}}
AllowedIPs = 0.0.0.0/0, ::/0`)).
		Execute(&buf, map[string]string{
			"GeneratedAt":     generatedAt,
			"Name":            name,
			"PrivateKey":      key.String(),
			"IPv4":            ipv4,
			"PeerDns":         strings.Join(h.store.DNS(), ","),
			"ServerPublicKey": devConf.PublicKey,
			"PresharedKey":    presharedKey.String(),
			"ServerEndpoint":  fmt.Sprintf("%s:%d", h.ServerAddr, devConf.ListenPort),
		})
	if nil != err {
		return nil, fmt.Errorf("failed to create peer config from template: %v", err)
	}
	peerConfFileContent := buf.Bytes()

	err = h.wg.ConfigureDevice(h.wgDeviceName(), wgtypes.Config{
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey:                   key.PublicKey(),
				Remove:                      false,
				UpdateOnly:                  false,
				PresharedKey:                &presharedKey,
				Endpoint:                    nil,
				PersistentKeepaliveInterval: nil,
				ReplaceAllowedIPs:           true,
				AllowedIPs:                  []net.IPNet{{IP: net.ParseIP(ipv4), Mask: net.IPv4Mask(0xff, 0xff, 0xff, 0xff)}},
			},
		},
		ReplacePeers: false,
	})
	if nil != err {
		return peerConfFileContent, errors.Join(errWGDevicePeerAdd, err)
	}

	return peerConfFileContent, nil
}

func (h *Handler) getPeer(ctx context.Context, id int) ([]byte, bool, error) {
	peer, err := h.store.Peer(ctx, id)
	if nil != err {
		return nil, false, fmt.Errorf("failed to retrieve peer from store: %w", err)
	}

	confFileContent, err := os.ReadFile(h.ConfFilePath)
	if nil != err {
		return nil, false, fmt.Errorf("failed to read .conf file: %v", err)
	}
	devConf, err := parseDeviceConfFile(confFileContent)
	if nil != err {
		return nil, false, fmt.Errorf("failed to parse device .conf file: %v", err)
	}

	var buf bytes.Buffer
	err = template.Must(template.New("").Parse(`# {{.GeneratedAt}}
# {{.Name}}
[Interface]
PrivateKey = {{.PrivateKey}}
Address = {{.IPv4}}/32
DNS = {{.PeerDns}}

[Peer]
PublicKey = {{.ServerPublicKey}}
PresharedKey = {{.PresharedKey}}
Endpoint = {{.ServerEndpoint}}
AllowedIPs = 0.0.0.0/0, ::/0`)).
		Execute(&buf, map[string]string{
			"GeneratedAt":     peer.DateCreated,
			"Name":            peer.Name,
			"PrivateKey":      peer.PrivateKey,
			"IPv4":            peer.AllowedIPs.IPv4,
			"PeerDns":         strings.Join(h.store.DNS(), ","),
			"ServerPublicKey": devConf.PublicKey,
			"PresharedKey":    peer.PSK,
			"ServerEndpoint":  fmt.Sprintf("%s:%d", h.ServerAddr, devConf.ListenPort),
		})
	if nil != err {
		return nil, false, fmt.Errorf("failed to create peer config from template: %v", err)
	}

	return buf.Bytes(), peer.IsActive, nil
}

func (h *Handler) deactivatePeer(ctx context.Context, id int) error {
	if err := h.store.DeactivatePeer(ctx, id); nil != err {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return fmt.Errorf("failed to persist peer deactivation state: %v", err)
	}
	peer, err := h.store.Peer(ctx, id)
	if nil != err {
		return fmt.Errorf("failed to retrieve peer info: %v", err)
	}

	pubKey, err := wgtypes.ParseKey(peer.PublicKey)
	if nil != err {
		return fmt.Errorf("failed to parse peer public key: %v", err)
	}
	err = h.wg.ConfigureDevice(h.wgDeviceName(), wgtypes.Config{
		ReplacePeers: false,
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey: pubKey,
				Remove:    true,
			},
		},
	})
	if nil != err {
		return fmt.Errorf("failed to remove deactivated peer from WireGuard device: %v", err)
	}

	return nil
}

func (h *Handler) activatePeer(ctx context.Context, id int) error {
	if err := h.store.ActivatePeer(ctx, id); nil != err {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return fmt.Errorf("failed to persist peer activation state: %v", err)
	}
	peer, err := h.store.Peer(ctx, id)
	if nil != err {
		return fmt.Errorf("failed to retrieve peer info: %v", err)
	}

	pubKey, err := wgtypes.ParseKey(peer.PublicKey)
	if nil != err {
		return fmt.Errorf("failed to parse peer public key: %v", err)
	}

	psk, err := wgtypes.ParseKey(peer.PSK)
	if nil != err {
		return fmt.Errorf("failed to parse peer preshared key: %v", err)
	}

	_, ipNet, err := net.ParseCIDR(peer.AllowedIPs.IPv4 + "/32")
	if nil != err {
		return fmt.Errorf("failed to parse peer ipv4 address: %v", err)
	}
	allowedIPs := []net.IPNet{*ipNet}

	err = h.wg.ConfigureDevice(h.wgDeviceName(), wgtypes.Config{
		ReplacePeers: false,
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey:    pubKey,
				PresharedKey: &psk,
				AllowedIPs:   allowedIPs,
				Remove:       false,
			},
		},
	})
	if nil != err {
		return fmt.Errorf("failed to add activated peer to WireGuard device: %v", err)
	}

	return nil
}

type PeersListItem struct {
	ID            int
	PublicKey     string
	AllowedIPs    string
	Name          string
	DownloadBytes int64
	UploadBytes   int64
	IsActive      bool
}

type PeersList struct {
	Total      int
	TotalPages int
	Peers      []PeersListItem
}

func (h *Handler) listPeers(ctx context.Context, page int) (*PeersList, error) {
	peers, err := h.store.Peers(ctx, db.WithPageNo(page), db.WithPageSize(10))
	if nil != err {
		if errors.Is(err, os.ErrNotExist) {
			return &PeersList{Total: 0, TotalPages: 0, Peers: nil}, nil
		}

		return nil, fmt.Errorf("failed to retrieve peers list: %v", err)
	}

	dev, err := h.wg.Device(h.wgDeviceName())
	if nil != err {
		return nil, fmt.Errorf("failed to get WireGuard device info: %w", err)
	}

	peersListItems := make([]PeersListItem, 0, len(peers.Items))
	for _, peer := range peers.Items {
		if !peer.IsActive {
			peersListItems = append(peersListItems, PeersListItem{
				ID:            peer.ID,
				PublicKey:     peer.PublicKey,
				AllowedIPs:    fmt.Sprintf("%s/32", peer.AllowedIPs.IPv4),
				Name:          peer.Name,
				DownloadBytes: 0,
				UploadBytes:   0,
				IsActive:      false,
			})
			continue
		}
		for _, wgPeer := range dev.Peers {
			if wgPeer.PublicKey.String() == peer.PublicKey {
				peersListItems = append(peersListItems, PeersListItem{
					ID:            peer.ID,
					PublicKey:     wgPeer.PublicKey.String(),
					AllowedIPs:    fmt.Sprintf("%s/32", peer.AllowedIPs.IPv4),
					Name:          peer.Name,
					DownloadBytes: wgPeer.TransmitBytes,
					UploadBytes:   wgPeer.ReceiveBytes,
					IsActive:      true,
				})
				break
			}
		}
	}

	return &PeersList{Total: peers.Total, TotalPages: peers.TotalPages, Peers: peersListItems}, nil
}

func (h *Handler) wgDeviceName() string {
	return strings.TrimRight(filepath.Base(h.ConfFilePath), ".conf")
}
